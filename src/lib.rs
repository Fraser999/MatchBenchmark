#![allow(dead_code)]
#![feature(test)]

extern crate rand;
extern crate test;

static REPEAT_COUNT: u64 = 18446744073709551615;

#[derive(Clone)]
pub enum ExternalRequest {
    Get(u64),
    Put(u64),
    Post(u64),
    Delete(u64),
}

fn handle_get(request: u64, count: &mut u64) {
    *count += request;
}

fn handle_put(request: u64, count: &mut u64) {
    *count += request;
}

fn handle_post(request: u64, count: &mut u64) {
    *count += request;
}

fn handle_delete(request: u64, count: &mut u64) {
    *count += request;
}

fn handler1(request: ExternalRequest, count: &mut u64) {
    match &request {
        &ExternalRequest::Get(get_request) => handle_get(get_request, count),
        &ExternalRequest::Put(put_request) => handle_put(put_request, count),
        &ExternalRequest::Post(post_request) => handle_post(post_request, count),
        &ExternalRequest::Delete(delete_request) => handle_delete(delete_request, count),
    }
}

#[bench]
fn bench1(bencher: &mut ::test::Bencher) {
    let mut get_count = 0u64;
    let mut put_count = 0u64;
    let mut post_count = 0u64;
    let mut delete_count = 0u64;
    let rand = ::rand::random::<u64>();

    bencher.iter(|| {
        for _ in 0..REPEAT_COUNT {
            handler1(ExternalRequest::Get(rand), &mut get_count);
            handler1(ExternalRequest::Put(rand), &mut put_count);
            handler1(ExternalRequest::Post(rand), &mut post_count);
            handler1(ExternalRequest::Delete(rand), &mut delete_count);
        }
    });
    // println!("Gets: {}, Puts: {}, Posts: {}, Deletes: {}", get_count, put_count, post_count, delete_count);
}

#[test]
fn test1() {
    let mut get_count = 0u64;
    let mut put_count = 0u64;
    let mut post_count = 0u64;
    let mut delete_count = 0u64;
    let rand = ::rand::random::<u64>();

    for _ in 0..REPEAT_COUNT {
        handler1(ExternalRequest::Get(rand), &mut get_count);
        handler1(ExternalRequest::Put(rand), &mut put_count);
        handler1(ExternalRequest::Post(rand), &mut post_count);
        handler1(ExternalRequest::Delete(rand), &mut delete_count);
    }
    print!("Gets: {}, Puts: {}, Posts: {}, Deletes: {} ... ", get_count, put_count, post_count, delete_count);
}









fn handle_get2(request: &ExternalRequest, count: &mut u64) -> Option<()> {
    match request {
        &ExternalRequest::Get(get_request) => {
            handle_get(get_request, count);
            Some(())
        },
        _ => None,
    }
}

fn handle_put2(request: &ExternalRequest, count: &mut u64) -> Option<()> {
    match request {
        &ExternalRequest::Put(put_request) => {
            handle_put(put_request, count);
            Some(())
        },
        _ => None,
    }
}

fn handle_post2(request: &ExternalRequest, count: &mut u64) -> Option<()> {
    match request {
        &ExternalRequest::Post(post_request) => {
            handle_post(post_request, count);
            Some(())
        },
        _ => None,
    }
}

fn handle_delete2(request: &ExternalRequest, count: &mut u64) -> Option<()> {
    match request {
        &ExternalRequest::Delete(delete_request) => {
            handle_delete(delete_request, count);
            Some(())
        },
        _ => None,
    }
}

fn handler2(request: ExternalRequest, count: &mut u64) {
    let _ = handle_get2(&request, count)
                .or_else(|| handle_put2(&request, count))
                .or_else(|| handle_post2(&request, count))
                .or_else(|| handle_delete2(&request, count));
}

#[bench]
fn bench2(bencher: &mut ::test::Bencher) {
    let mut get_count = 0u64;
    let mut put_count = 0u64;
    let mut post_count = 0u64;
    let mut delete_count = 0u64;
    let rand = ::rand::random::<u64>();

    bencher.iter(|| {
        for _ in 0..REPEAT_COUNT {
            handler2(ExternalRequest::Get(rand), &mut get_count);
            handler2(ExternalRequest::Put(rand), &mut put_count);
            handler2(ExternalRequest::Post(rand), &mut post_count);
            handler2(ExternalRequest::Delete(rand), &mut delete_count);
        }
    });
    // println!("Gets: {}, Puts: {}, Posts: {}, Deletes: {}", get_count, put_count, post_count, delete_count);
}

#[test]
fn test2() {
    let mut get_count = 0u64;
    let mut put_count = 0u64;
    let mut post_count = 0u64;
    let mut delete_count = 0u64;
    let rand = ::rand::random::<u64>();

    for _ in 0..REPEAT_COUNT {
        handler2(ExternalRequest::Get(rand), &mut get_count);
        handler2(ExternalRequest::Put(rand), &mut put_count);
        handler2(ExternalRequest::Post(rand), &mut post_count);
        handler2(ExternalRequest::Delete(rand), &mut delete_count);
    }
    print!("Gets: {}, Puts: {}, Posts: {}, Deletes: {} ... ", get_count, put_count, post_count, delete_count);
}
